# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from .models import formData
from .models import contactData
from .models import newsletterData
from .models import metisUser

class Form(ModelForm):
	class Meta:
		model = formData
		fields = '__all__'
	amount = forms.IntegerField(label='Kwota finansowania (PLN)')
	duration = forms.IntegerField(label='Okres finansowania (mies.)')
	purpose = forms.ChoiceField(choices=(('LKO', 'Linia kredytowa odnawialna'),('KO', 'Kredyt obrotowy'),('KI', 'Kredyt inwestycyjny'),('ZST', 'Zakup środkow trwałych'),('ZN','Zakup nieruchomości'),('ALL','Wszystkie'), ('INNE','Inne (opisz poniżej)')), label='Cel finansowania',)
	time_operating = forms.IntegerField(label='Czas działalności firmy (mies.)')
	tellmore = forms.CharField(widget=forms.Textarea(attrs={'cols': 60, 'rows': 5}), required=False, label='Powiedz nam coś więcej o potrzebach finansowych Twojej firmy (nieobowiązkowe)',max_length=300)
	legal_type = forms.ChoiceField(choices=(('ZOO', 'sp. z.o.o.'),('SCYW', 'Spółka cywilna'),('SAKC', 'Spółka akcyjna'), ('INNY','Inny')), label='Rodzaj firmy',)
	email = forms.CharField(label='Adres E-Mail', max_length=30)

class ContactForm(ModelForm):
	class Meta:
		model = contactData
		fields = '__all__'
	name = forms.CharField(max_length=100)
	email = forms.CharField(max_length=30)
	phone = forms.CharField(max_length=15)
	title = forms.CharField(max_length=100)
	message = forms.CharField()

class NewsletterForm(ModelForm):
	class Meta:
		model = newsletterData
		fields = '__all__'
	email = forms.CharField(max_length=50)

class metisForm(ModelForm):
	class Meta:
		model = metisUser
		fields = '__all__'
	email = forms.CharField(max_length=50)
