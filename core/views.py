# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from .forms import Form
from .models import formData
from .models import article
from .forms import ContactForm
from .forms import NewsletterForm
from .forms import metisForm
from .controllers import getLenders
from .controllers import getArticles
from .controllers import getAllArticles
from random import randint

def renderCore(request):
	inpform = Form()
	return render(request, 'index.html', {'form': inpform, 'articles': getArticles(count = 4), 'newsletterform': NewsletterForm()})

def renderResults(request):
	if not request.method == 'POST':
		return render(request, 'error.html', {'errortext': 'Formularz nie został przesłany!'})
	postform = Form(request.POST)
	if postform.is_valid():
		postform.save()
	lender_array = getLenders(postform)
	return render(request, 'results.html', {'form': postform, 'array': lender_array, 'popup': randint(0,9)})

def renderArticle(request, id):
	art = article.objects.get(id__exact=int(id))
	return render(request, 'article_view.html', {'form': Form(), 'article': art})

def renderArticles(request):
	articles = getAllArticles()
	return render(request, 'articles.html', {'articles': articles})

def renderContact(request):
	contactform = ContactForm()
	return render(request, 'contact.html', {'form': ContactForm()})

def renderNewsletter(request):
	if request.method == 'POST':
		postform = NewsletterForm(request.POST)
		if postform.is_valid():
			postform.save()
			return render(request, 'newsletter.html', {'post': True})
	else:
		return render(request, 'newsletter.html', {'post': False, 'from': NewsletterForm()})

def renderSumbitMessage(request):
	if not request.method == 'POST':
		return render(request, 'error.html', {'errortext': 'Wiadomość nie została przesłana!'})
	postform = ContactForm(request.POST)
	if postform.is_valid():
		postform.save()
	else:
		return render(request, 'error.html', {'errortext': 'Wiadomość nie została przesłana!'})
	return render(request, 'sumbitmessage.html')

def renderBlog(request):
	response = HttpResponse()
	response['X-Accel-Redirect'] = '/root/blog/index.php'
	return response

def renderDownloads(request):
	return render(request, 'download.html')

def renderAbout(request):
	return render(request, 'about.html')

def renderMetis(request):
	if request.method == 'POST':
		postform = metisForm(request.POST)
		if postform.is_valid():
			postform.save()
			return render(request, 'metisflow.html', {'post': True})
	else:
		return render(request, 'metisflow.html', {'post': False, 'from': metisForm()})

# Create your views here.
