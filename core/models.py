# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone

class formData(models.Model):
	amount = models.IntegerField()
	duration = models.IntegerField()
	purpose = models.CharField(max_length=30, default='INNE')
	time_operating = models.IntegerField()
	tellmore = models.CharField(max_length=300    )
	legal_type = models.CharField(max_length=30, default='ZOO')
	email = models.CharField(max_length=30)
	datetime = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.datetime.strftime("%Y-%m-%d %H:%M:%S") + '  |  ' + self.email

	class Meta:
		db_table = 'clients'
		verbose_name = 'Wynik formularza'
		verbose_name_plural = 'Wyniki formularza'

class lenderData(models.Model):
	listInResults = models.BooleanField()
	isOnline = models.BooleanField()
	name = models.CharField(max_length = 128)
	logofname = models.CharField(max_length = 128)
	website = models.CharField(max_length = 100)
	purpose = models.CharField(max_length = 30, default='INNE')
	isForIndividualsOnly = models.BooleanField()
	minDuration = models.FloatField()
	maxDuration = models.FloatField()
	minAmount = models.FloatField()
	maxAmount = models.FloatField()
	timeInOp = models.IntegerField()
	minAPR = models.FloatField(null = True, blank = True)
	maxAPR = models.FloatField(null = True, blank = True)
	isBIKchecked = models.CharField(max_length = 30, null = True, blank = True)
	isKRDchecked = models.CharField(max_length = 30, null = True, blank = True)
	otherReq = models.CharField(max_length = 500, null = True, blank = True)
	isCollateral = models.CharField(max_length = 30, null = True, blank = True)
	timeToDecide = models.IntegerField(null = True, blank = True)
	moreInfo = models.CharField(max_length = 500, null = True, blank = True)

	def __str__(self):
		return self.name

	class Meta:
		db_table = 'lenders'
		verbose_name = 'Firma'
		verbose_name_plural = 'Firmy'

class article(models.Model):
	title = models.CharField(max_length = 128)
	description = models.CharField(max_length = 100, null = True, blank = True)
	text = models.TextField()
	author = models.CharField(max_length = 30, null = True, blank = True)
	date = models.DateTimeField(auto_now=True)
	lender_name = models.CharField(max_length = 30)

	def __str__(self):
		return self.title

	class Meta:
		db_table = 'article'
		verbose_name = 'Artykuł'
		verbose_name_plural = 'Artykuły'

class contactData(models.Model):
	name = models.CharField(max_length=100)
	email = models.CharField(max_length=30)
	phone = models.CharField(max_length=15)
	title = models.CharField(max_length=100)
	message = models.TextField()
	datetime = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.datetime.strftime("%Y-%m-%d %H:%M:%S") + '  |  ' + self.email

	class Meta:
		db_table = 'contact_messages'
		verbose_name = 'Wiadomość'
		verbose_name_plural = 'Wiadomości'

class newsletterData(models.Model):
	email = models.CharField(max_length=50)
	datetime = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.datetime.strftime("%Y-%m-%d %H:%M:%S") + '  |  ' + self.email

	class Meta:
		db_table = 'newsletter_emails'
		verbose_name = 'E-mail kilenta newslettera'
		verbose_name_plural = 'E-maile klientów newsletterów'

class metisUser(models.Model):
	email = models.CharField(max_length=50)
	datetime = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.datetime.strftime("%Y-%m-%d %H:%M:%S") + '  |  ' + self.email

	class Meta:
		db_table = 'metis_users'
		verbose_name = 'Użytkownik Metisflow'
		verbose_name_plural = 'Użytkownicy Metisflow'
