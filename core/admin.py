from django.contrib import admin
from .models import formData
from .models import lenderData
from .models import contactData
from .models import article
from .models import newsletterData
from .models import metisUser

admin.site.register(formData)
admin.site.register(lenderData)
admin.site.register(article)
admin.site.register(contactData)
admin.site.register(newsletterData)
admin.site.register(metisUser)

# Register your models here.
