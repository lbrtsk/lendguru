# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-19 21:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_lenderdata_purpose'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lenderdata',
            name='purpose',
            field=models.CharField(default=b'INNE', max_length=30),
        ),
    ]
