from .models import lenderData
from .models import article

def getLenders(formData):
	ret = []
	score = []

	for e in lenderData.objects.all():
		currentScore = 0

		if not e.timeInOp <= formData.cleaned_data['time_operating']:
			continue

		if e.minAmount <= formData.cleaned_data['amount'] and e.maxAmount >= formData.cleaned_data['amount']:
			currentScore = currentScore + 5
			print("%s amount matches", e.name)

		if e.minDuration <= formData.cleaned_data['duration'] and e.maxDuration >= formData.cleaned_data['duration']:
			currentScore = currentScore + 3
			print("%s duration matches", e.name)

		if e.purpose == formData.cleaned_data['purpose'] or e.purpose == "ALL":
			currentScore = currentScore + 1
			print("%s purpose matches", e.name)

		if len(ret) > 0:
			for i in range(0, len(ret)):
				if currentScore > score[i]:
					ret.insert(i, e)
					score.insert(i, currentScore)
					i = -1
					break
			if not i == -1:
				ret.append(e)
				score.append(currentScore)
		else:
			ret.append(e)
			score.append(currentScore)

		print("%s score: %d", e.name, currentScore)

	return ret

def getArticles(count):
	ret = []
	n = 0

	for art in article.objects.all():
		ret.append(art)
		n = n + 1
		if n > count:
			break

	return ret

def getAllArticles():
	ret = []

	for art in article.objects.all():
		ret.append(art)

	return ret
