"""lendguru URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.http import HttpResponse
import core.views

urlpatterns = [
	url(r'^$', core.views.renderCore, name='index'),
	url(r'^admin/', admin.site.urls),
	url(r'^form/', core.views.renderResults, name='results'),
	url(r'^articles/(?P<id>[0-9]{4})/$', core.views.renderArticle, name='article_view'),
	url(r'^all_articles/$', core.views.renderArticles, name='articles_view'),
	url(r'^contact/', core.views.renderContact, name='contact'),
	url(r'^sumbitmessage/', core.views.renderSumbitMessage, name='sumbitmessage'),
	url(r'^blog/', core.views.renderBlog, name='blog'),
	url(r'^downloads/', core.views.renderDownloads, name='downloads'),
	url(r'^about/', core.views.renderAbout, name='about'),
	url(r'^newsletter/', core.views.renderNewsletter, name='newsletter'),
	url(r'^metisflow/', core.views.renderMetis, name='metis')
]
